#!/usr/bin/python3
# pylint: disable=useless-return
'''
An IMAP client to fetch and analyse TLSRPT reports in a mailbox
'''

import os
import sys
import getpass
import datetime
import argparse
import gzip
import json

import imap_tools
import tabulate

# pylint: disable=line-too-long
parser = argparse.ArgumentParser(description='tlsrpt')
parser.add_argument('--username', type=str, help='username to connect to IMAP server')
parser.add_argument('--server',   type=str, help='IMAP server to connect to')
parser.add_argument('--folder',   type=str, help='IMAP folder to search', default='INBOX')
parser.add_argument('--unread',   action='store_true', help='only scan unread report mails and mark as read')
parser.add_argument('--no-mark-read', action='store_true', help='do not mark as read (only used in conjunction with --unread)')
parser.add_argument('--since',    type=str, help='show reports received since YYYY-MM-DD')
parser.add_argument('--until',    type=str, help='show reports received until YYYY-MM-DD')
parser.add_argument('--subject',  action='store_true', help='show mail subject of report')
parser.add_argument('--failures', action='store_true', help='print only failures')
parser.add_argument('--verbose',  action='store_true', help='be verbose')
args = parser.parse_args()
# pylint: enable=line-too-long

password = os.environ.get('TLSRPT_PASSWORD', None)
if password is None:
    password = getpass.getpass()

def warn(msg):
    '''
    print a warning to stderr
    '''
    print(msg, file=sys.stderr)
    return

CRLF = '\r\n'

searchfilter = {
    'subject': 'Report Domain:',
}
if args.unread:
    searchfilter['seen'] = False
if args.since:
    searchfilter['date_gte'] = datetime.date.fromisoformat(args.since)
if args.until:
    searchfilter['date_lt'] = datetime.date.fromisoformat(args.until) + datetime.timedelta(days=1)

reports = {}

def handle_attachments(msg):
    '''
    handler for every message found when fetching mails from mailbox
    '''
    for attachment in msg.attachments:
        if attachment.content_type not in ['application/tlsrpt+gzip']:
            warn(
                f'Unsupported content type in message {msg.date} '
                f'"{msg.subject.replace(CRLF, " ")}": '
                f'{attachment.content_type}, skipping'
            )
            continue
        content = gzip.decompress(attachment.payload).decode()
        cur_report = json.loads(content)
        if msg.subject in reports:
            warn(f'Found second mail with subject "{msg.subject.replace(CRLF, " ")}"')
        reports[msg.subject] = cur_report
    return

with imap_tools.MailBox(args.server).login(args.username, password) as mailbox:
    all_folders = {
        f.name: f
        for f in mailbox.folder.list()
    }
    if args.folder not in all_folders:
        raise ValueError(
            f'Folder "{args.folder}" does not exist: '
            f'{sorted(all_folders.keys())}'
        )
    mailbox.folder.set(args.folder)
    msgs = mailbox.fetch(
        imap_tools.AND(**searchfilter),
        headers_only=False,
        bulk=True,
        mark_seen=False,
        charset='UTF8'
    )
    mark_seen = []
    for m in msgs:
        handle_attachments(m)
        if args.unread and not args.no_mark_read:
            mark_seen.append(m.uid)
    if mark_seen:
        mailbox.flag(mark_seen, flag_set={imap_tools.consts.MailMessageFlags.SEEN}, value=True)

# pylint: disable=pointless-string-statement
'''
{
    "organization-name": "Google Inc.",
    "date-range": {
        "start-datetime": "2023-09-14T00:00:00Z",
        "end-datetime": "2023-09-14T23:59:59Z"
    },
    "contact-info": "smtp-tls-reporting@google.com",
    "report-id": "2023-09-14T00:00:00Z_mpi-klsb.mpg.de",
    "policies": [
        {
            "policy": {
                "policy-type": "no-policy-found",
                "policy-domain": "mpi-klsb.mpg.de"
            },
            "summary": {
                "total-successful-session-count": 53,
                "total-failure-session-count": 0
            }
        }
    ]
}
'''
# pylint: enable=pointless-string-statement
table = []
for subject, report in reports.items():
    org = report['organization-name']
    start = report['date-range']['start-datetime']
    for p in report['policies']:
        ptype     = p['policy']['policy-type']
        domain    = p['policy']['policy-domain']
        succeeded = p['summary']['total-successful-session-count']
        failed    = p['summary']['total-failure-session-count']
        row = [ start, org, domain, ptype, succeeded, failed ]
        if not failed and args.failures:
            continue
        if args.subject:
            row.extend([subject.replace('\r\n', ' ')])
        if not args.verbose or not 'failure-details' in p:
            table.append(row)
            continue
        for detail in p['failure-details']:
            result_type = detail.get('result-type',           'no result-type')
            sender      = detail.get('sending-mta-ip',        'no sending-mta-ip')
            receiver_ip = detail.get('receiving-ip',          'no receiving-ip')
            receiver    = detail.get('receiving-mx-hostname', 'no receiving-mx-hostname')
            count       = detail.get('failed-session-count',  -1)
            table.append(
                row +
                [result_type, sender, receiver_ip, receiver, count]
            )

if not table:
    sys.exit(0)
headers = 'start org domain policy-type succeeded failed'.upper().split()
if args.subject:
    headers.append('SUBJECT')
if args.verbose:
    headers += 'result_type sender receiver_ip receiver count'.split()
print(
    tabulate.tabulate(
        sorted(table),
        headers=headers,
    )
)

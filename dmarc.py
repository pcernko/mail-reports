#!/usr/bin/python3
# pylint: disable=useless-return
'''
An IMAP client to fetch and analyse dmarc reports in a mailbox
'''

import os
import sys
import getpass
import datetime
import argparse
import gzip
import zipfile
import io
import xml.etree.ElementTree
import socket
import re

import imap_tools
import tabulate

# pylint: disable=line-too-long
parser = argparse.ArgumentParser(description='dmarc')
parser.add_argument('--username', type=str, help='username to connect to IMAP server')
parser.add_argument('--server',   type=str, help='IMAP server to connect to')
parser.add_argument('--folder',   type=str, help='IMAP folder to search', default='INBOX')
parser.add_argument('--unread',   action='store_true', help='only scan unread report mails and mark as read')
parser.add_argument('--no-mark-read', action='store_true', help='do not mark as read (only used in conjunction with --unread)')
parser.add_argument('--since',    type=str, help='show reports received since YYYY-MM-DD')
parser.add_argument('--until',    type=str, help='show reports received until YYYY-MM-DD')
parser.add_argument('--subject',  action='store_true', help='show mail subject of report')
parser.add_argument('--failures', action='store_true', help='print only failures')
parser.add_argument('--ignore-spf', action='store_true', help='print only failures that are not SPF')
parser.add_argument('--ignore-google-relays', action='store_true', help='ignore SPF failures from Google with Google mail frontends as source')
parser.add_argument('--sources',  type=str, help='own sources (space seperated), ignore other sources')
parser.add_argument('--verbose',  action='store_true', help='be verbose')
args = parser.parse_args()
# pylint: enable=line-too-long

password = os.environ.get('DMARC_PASSWORD', None)
if password is None:
    password = getpass.getpass()

def warn(msg):
    '''
    print a warning to stderr
    '''
    print(msg, file=sys.stderr)
    return

CRLF = '\r\n'

searchfilter = {
    'subject': 'Report Domain:',
}
if args.unread:
    searchfilter['seen'] = False
if args.since:
    searchfilter['date_gte'] = datetime.date.fromisoformat(args.since)
if args.until:
    searchfilter['date_lt'] = datetime.date.fromisoformat(args.until) + datetime.timedelta(days=1)

sources = args.sources.split() if args.sources else None

reports = {}

def handle_attachments(msg):
    '''
    handler for every message found when fetching mails from mailbox
    '''
    for attachment in msg.attachments:
        if attachment.content_type not in [
                'application/octet-stream',
                'application/gzip',
                'application/zip'
        ]:
            warn(
                f'Unsupported content type in message {msg.date} '
                f'"{msg.subject.replace(CRLF, " ")}": '
                f'{attachment.content_type}, skipping'
            )
            continue
        filename = attachment.filename
        content = attachment.payload
        if filename.endswith('...'):
            # bug in email module, incorrectly parsing:
            # Date: Sat, 8 Feb 2025 01:27:07 +0100
            # MIME-Version: 1.0
            # Content-Type: application/gzip;
            #  name=ruhr-uni-bochum.de!mpi-inf.mpg.de!1738886400!1738972799!18651.xml.gz
            # Content-Transfer-Encoding: base64
            # Content-Disposition: inline;
            #  filename*0=ruhr-uni-bochum.de!mpi-inf.mpg.de!1738886400!1738972799!18651.xml;
            #  filename*1=.gz;
            #  filename=ruhr-uni-bochum.de!mpi-inf.mpg.de!1738886400!1738972799!18651.xm...
            # the bug is, that email.message.Message.get_params() returns a LIST of tuples where:
            # [('inline', ''), ('filename', 'ruhr-uni-bochum.de!mpi-inf.mpg.de!1738886400!1738972799!18651.xm...'), ('filename', 'ruhr-uni-bochum.de!mpi-inf.mpg.de!1738886400!1738972799!18651.xml.gz')]
            # this is getting to complicate to file a bug (against python!)
            # workraround:
            # https://list.mailop.org/private/mailop/2025-February/030427.html
            if content[:3] == b"\x1f\x8b\x08":
                # GZIP content
                filename += '.gz'
            elif content[:4] == b"\x50\x4b\x03\x04":
                # ZIP content
                filename += '.zip'
            elif content[:19] == b'<?xml version="1.0"':
                # XML content
                filename += '.xml'
        if filename.endswith('.gz'):
            content = gzip.decompress(content)
        elif filename.endswith('.zip'):
            with zipfile.ZipFile(io.BytesIO(content)) as archive:
                members = archive.infolist()
                if len(members) != 1:
                    warn(
                        f'Unsupported zip file in message {msg.date} '
                        f'"{msg.subject.replace(CRLF, " ")}": '
                        f'not exactly one archive member: '
                        f'{list(m.filename for m in members)}'
                    )
                    continue
                content = archive.read(members[0])
        try:
            content = content.decode()
        except UnicodeDecodeError:
            warn(
                f'Failed to decode content in message {msg.date} '
                f'"{msg.subject.replace(CRLF, " ")}" '
                f'UnicodeDecodeError, skipping'
            )
            continue
        try:
            cur_report = xml.etree.ElementTree.fromstring(content)
        except xml.etree.ElementTree.ParseError as xml_ex:
            warn(
                f'unparsable XML in message {msg.date} '
                f'"{msg.subject.replace(CRLF, " ")}": {xml_ex}'
            )
            continue
        if msg.subject in reports:
            pass # warn(f'Found second mail with subject "{msg.subject.replace(CRLF, " ")}"')
        reports[msg.subject] = cur_report
    return

with imap_tools.MailBox(args.server).login(args.username, password) as mailbox:
    all_folders = {
        f.name: f
        for f in mailbox.folder.list()
    }
    if args.folder not in all_folders:
        raise ValueError(
            f'Folder "{args.folder}" does not exist: '
            f'{sorted(all_folders.keys())}'
        )
    mailbox.folder.set(args.folder)
    msgs = mailbox.fetch(
        imap_tools.AND(**searchfilter),
        headers_only=False,
        bulk=True,
        mark_seen=False,
        charset='UTF8'
    )
    mark_seen = []
    for m in msgs:
        handle_attachments(m)
        if args.unread and not args.no_mark_read:
            mark_seen.append(m.uid)
    if mark_seen:
        mailbox.flag(mark_seen, flag_set={imap_tools.consts.MailMessageFlags.SEEN}, value=True)

# pylint: disable=pointless-string-statement
'''
<?xml version="1.0"?>
<feedback>
	<version>0.1</version>
	<report_metadata>
		<org_name>AMAZON-SES</org_name>
		<email>postmaster@amazonses.com</email>
		<report_id>92c851b1-2715-4604-9b62-cccba217cb35</report_id>
		<date_range>
			<begin>1695513600</begin>
			<end>1695600000</end>
		</date_range>
	</report_metadata>
	<policy_published>
		<domain>errror.org</domain>
		<adkim>s</adkim>
		<aspf>s</aspf>
		<p>reject</p>
		<sp>reject</sp>
		<pct>100</pct>
		<fo>1:d:s</fo>
	</policy_published>
	<record>
		<row>
			<source_ip>195.191.197.182</source_ip>
			<count>3</count>
			<policy_evaluated>
				<disposition>none</disposition>
				<dkim>pass</dkim>
				<spf>pass</spf>
			</policy_evaluated>
		</row>
		<identifiers>
			<envelope_from>errror.org</envelope_from>
			<header_from>errror.org</header_from>
		</identifiers>
		<auth_results>
			<dkim>
				<domain>errror.org</domain>
				<result>pass</result>
			</dkim>
			<spf>
				<domain>errror.org</domain>
				<result>pass</result>
			</spf>
		</auth_results>
	</record>
</feedback>
'''
# pylint: enable=pointless-string-statement
def xmlget(element, path):
    '''
    extract the XML value from the given Xpath
    '''
    tag = element.find(path)
    if tag is None:
        return ''
    return tag.text

rdns = {}
def ip2name(ip_addr):
    '''
    resolve IP address to dns name
    '''
    try:
        hostname = socket.getfqdn(ip_addr)
    # pylint: disable=broad-exception-caught
    except Exception as ex:
        warn(f'Failed to resolve IP {ip_addr} to a name: {ex}')
        return ip_addr
    return rdns.setdefault(ip_addr, hostname)

table = []
for subject, report in reports.items():
    NSPREFIX = '{urn:ietf:params:xml:ns:dmarc-2.0}' \
        if report.tag.startswith('{urn:ietf:params:xml:ns:dmarc-2.0}') else \
           ''
    org    = xmlget(report, f'./{NSPREFIX}report_metadata/{NSPREFIX}org_name')
    domain = xmlget(report, f'./{NSPREFIX}policy_published/{NSPREFIX}domain')
    start  = xmlget(report, f'./{NSPREFIX}report_metadata/{NSPREFIX}date_range/{NSPREFIX}begin')

    try:
        start = datetime.datetime.fromtimestamp(int(start))
    except ValueError as ve:
        warn(f'Got ValueError "{ve}" with start="{start}", org="{org}", domain="{domain}"')
        md = report.find('./{NSPREFIX}report_metadata')
        warn(xml.etree.ElementTree.tostring(md).decode())
        for sub in [ 'date_range', 'report_id', 'extra_contact_info', 'email', ]:
            subtag = md.find(f'./{NSPREFIX}{sub}')
            warn(
                f'{sub} is: '+
                (
                    'None' \
                    if subtag is None else \
                    xml.etree.ElementTree.tostring(subtag).decode().rstrip()
                )
            )
        warn('skipping')
        continue

    row = [ start, org, domain, ]
    if args.subject:
        row.extend([subject.replace('\r\n', ' ').replace('\t', ' ').rstrip()])
    for r in report.findall(f'{NSPREFIX}record'):
        # pylint: disable=line-too-long
        source_ip   = xmlget(r, f'./{NSPREFIX}row/{NSPREFIX}source_ip')
        count       = xmlget(r, f'./{NSPREFIX}row/{NSPREFIX}count')
        disposition = xmlget(r, f'./{NSPREFIX}row/{NSPREFIX}policy_evaluated/{NSPREFIX}disposition')
        reason      = xmlget(r, f'./{NSPREFIX}row/{NSPREFIX}policy_evaluated/{NSPREFIX}reason/{NSPREFIX}type')
        comment     = xmlget(r, f'./{NSPREFIX}row/{NSPREFIX}policy_evaluated/{NSPREFIX}reason/{NSPREFIX}comment')
        dkim        = xmlget(r, f'./{NSPREFIX}auth_results/{NSPREFIX}dkim/{NSPREFIX}result')
        dkim_domain = xmlget(r, f'./{NSPREFIX}auth_results/{NSPREFIX}dkim/{NSPREFIX}domain')
        spf         = xmlget(r, f'./{NSPREFIX}auth_results/{NSPREFIX}spf/{NSPREFIX}result')
        spf_domain  = xmlget(r, f'./{NSPREFIX}auth_results/{NSPREFIX}spf/{NSPREFIX}domain')
        # pylint: enable=line-too-long
        name = ip2name(source_ip)
        if sources is not None and name not in sources and source_ip not in sources:
            continue
        source = name if name else source_ip
        dkim_success = dkim in [ 'pass', '', 'temperror' ]
        spf_success = True if args.ignore_spf else spf in [ 'pass', 'none', 'neutral', 'temperror' ]
        success = dkim_success and spf_success
        if args.failures and success:
            continue
        if args.ignore_google_relays and \
           org == 'google.com' and \
           dkim_success and \
           not spf_success and \
           re.match('^mail-.+.google.com$', source):
            continue
        add_row = row + [ source, count, disposition, reason, dkim_domain, dkim, spf_domain, spf, ]
        if args.verbose:
            add_row.append(comment)
        table.append(add_row)

if not table:
    sys.exit(0)
headers = 'start org domain'.upper().split()
if args.subject:
    headers.append('SUBJECT')
headers += 'source count disposition reason dkim_domain dkim spf_domain spf'.upper().split()
if args.verbose:
    headers += 'COMMENT'.split()
print(
    tabulate.tabulate(
        sorted(table),
        headers=headers,
    )
)

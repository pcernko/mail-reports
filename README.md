# mail-reports

Scripts to analyse DMARC and TLSRPT reports stored in an IMAP mailbox.

Use `./dmarc.py --help` resp. `./tlsrpt.py --help` to see the possible arguments.

Use `report.sh` for a regular cronjob after configuring `config.sh` (see `config.sh.sample` for an example).

## License

GPL.

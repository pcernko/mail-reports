#!/bin/bash

set -euo pipefail

program=$0
base=${program%/*}
if [ "$base" == "$program" ]; then
    base=$PWD
fi

# defaults
dmarc_mailserver=
dmarc_username=
dmarc_folder=
dmarc_args=()
dmarc_skip=
tlsrpt_mailserver=
tlsrpt_username=
tlsrpt_folder=
tlsrpt_args=()
tlsrpt_skip=
# overridden by config
if [ -r "$base"/config.sh ]; then
    . "$base"/config.sh
fi

if [ "$dmarc_skip" != "true" ]; then
    if [ -z "$dmarc_username" ] || [ -z "$dmarc_mailserver" ]; then
        echo "You must set dmarc_mailserver and dmarc_username or skip dmarc report analysis by setting dmarc_skip=true in $base/config.sh" >&2
    else
        cmd=(
            "$base"/dmarc.py
            --server "$dmarc_mailserver"
            --username "$dmarc_username"
        )
        if [ -n "$dmarc_folder" ]; then
            cmd+=(
                --folder "$dmarc_folder"
            )
        fi
        if [ -n "${dmarc_args[*]}" ]; then
            cmd+=( "${dmarc_args[@]}" )
        else
            cmd+=(
                --failures
                --unread
            )
        fi
        "${cmd[@]}"
    fi
fi

if [ "$tlsrpt_skip" != "true" ]; then
    if [ -z "$tlsrpt_username" ] || [ -z "$tlsrpt_mailserver" ]; then
        echo "You must set tlsrpt_mailserver and tlsrpt_username or skip tlsrpt report analysis by setting tlsrpt_skip=true in $base/config.sh" >&2
    else
        cmd=(
            "$base"/tlsrpt.py
            --server "$tlsrpt_mailserver"
            --username "$tlsrpt_username"
        )
        if [ -n "$tlsrpt_folder" ]; then
            cmd+=(
                --folder "$tlsrpt_folder"
            )
        fi
        if [ -n "${tlsrpt_args[*]}" ]; then
            cmd+=( "${tlsrpt_args[@]}" )
        else
            cmd+=(
                --failures
                --unread
            )
        fi
        "${cmd[@]}"
    fi
fi
